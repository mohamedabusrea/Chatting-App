var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'chattingApp'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://mohamedabusrea:chattingApp@ds151141.mlab.com:51141/chatting-app'
  },

  test: {
    root: rootPath,
    app: {
      name: 'chattingApp'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/chattest-v1-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'chattingApp'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/chattest-v1-production'
  }
};

module.exports = config[env];
