# Chatting-App

### Introduction:
This is an implementation of a simple chat app. it has the basic feature which is real-time chatting and has an extended part which is:
  - Edit/Delete messages
  - Each user has a profile page

### First, install the dependencies:

```bash
$ npm install
```

### To start:

```bash
$ npm start
```

## Tools
- [generator-express](https://github.com/petecoop/generator-express) : An express generator for Yeoman, based on the express command line tool.
- Express
- Vue.js
- MongoDB (mlab.com)
- Mongoose
- Socket.io
- [WebStorm](https://www.jetbrains.com/webstorm/?fromMenu) : JavaScript IDE

## Implemented Functions

- App already has 3 users to choose from
- App already has 3 Lists that all users already joined them
- You can open 2 tabs and login with different users and start chatting
- Your own message will be on the right side without your name Like "WhatsApp"
- You can hover on your messages and Edit/Delete them
- You can click on users to show more data about them like "WhatsApp"