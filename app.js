var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    config = require('./config/config'),
    glob = require('glob'),
    mongoose = require('mongoose');

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function() {
    throw new Error('unable to connect to database at ' + config.db);
});

var models = glob.sync(config.root + '/app/models/*.js');
models.forEach(function(model) {
    require(model);
});

app.use(function(req, res, next) {
    res.io = io;
    next();
})

module.exports = require('./config/express')(app, config);

server.listen(config.port, function() {
    console.log('Express server listening on port ' + config.port);
});

