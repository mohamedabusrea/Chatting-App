var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  users = mongoose.model('Users');

module.exports = function(app) {
  app.use('/', router);
};

router.get('/', function(req, res, next) {

  users.find(function(err, users) {
    if (err) return next(err);
    res.render('login', {users});
  });
});
