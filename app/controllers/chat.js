var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    users = mongoose.model('Users'),
    Lists = mongoose.model('Lists');

mongoose.Promise = global.Promise;

module.exports = function(app) {
    app.use('/', router);
};

router.get('/chat/:id', function(req, res, next) {
    users.find(function(err, users) {
        if (!users) res.redirect('/')
        else {
            if (err) return next(err);
            Lists.find(function(err, lists) {
                if (err) return next(err);

                res.io.on('connection', async function(socket) {
                    socket.removeAllListeners()
                    socket.emit('initMessages', {lists, users, userData: users.find(u => u.id == req.params.id)});
                    socket.on('syncMessages', function(listID, msg) {
                        Lists.findOne({_id: listID})
                             .then(function(list) {
                                 list.messages.push(msg);
                                 list.save()
                                     .then(function(l) {
                                         if (err) return next(err);
                                         res.io.emit('updateMessages', listID, l)
                                     });
                             });
                    });
                    socket.on('deleteMessage', function(listID, msgID) {
                        Lists.findOne({_id: listID})
                             .then(function(list) {
                                 list.messages.splice(list.messages.findIndex(m => m.id == msgID), 1);
                                 list.save()
                                     .then(function(l) {
                                         if (err) return next(err);
                                         res.io.emit('updateMessages', listID, l)
                                     });
                             });
                    })
                    socket.on('saveEdit', function(listID, msgID, msgContent) {
                        Lists.findOne({_id: listID})
                             .then(function(list) {
                                 list.messages.find(m => m.id == msgID).message = msgContent;
                                 list.save()
                                     .then(function(l) {
                                         if (err) return next(err);
                                         res.io.emit('updateMessages', listID, l)
                                     });
                             });
                    })
                })

                res.render('chat');
            });
        }
    })
});
