// Example model

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var ListsSchema = new Schema({
  name: String,
  messages: Array
});

mongoose.model('Lists', ListsSchema);

