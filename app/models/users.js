// Example model

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var UsersSchema = new Schema({
  name: String,
  lists: Array,
  data: Date
});

mongoose.model('Users', UsersSchema);

